<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A83458">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Die Dominico 8 Aug. 1641. Resolved upon the question by both Houses of Parliament, nemine contradicente, that this ensuing declaration shall be printed.</title>
    <author>England and Wales. Parliament.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A83458 of text R205415 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.3[11]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A83458</idno>
    <idno type="STC">Wing E2265</idno>
    <idno type="STC">Thomason 669.f.3[11]</idno>
    <idno type="STC">ESTC R205415</idno>
    <idno type="EEBO-CITATION">99864809</idno>
    <idno type="PROQUEST">99864809</idno>
    <idno type="VID">160569</idno>
    <idno type="PROQUESTGOID">2240921796</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A83458)</note>
    <note>Transcribed from: (Early English Books Online ; image set 160569)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 245:669f3[11])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Die Dominico 8 Aug. 1641. Resolved upon the question by both Houses of Parliament, nemine contradicente, that this ensuing declaration shall be printed.</title>
      <author>England and Wales. Parliament.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>by Robert Barker, printer to the Kings most Excellent Majesty: and by the assignes of John Bill,</publisher>
      <pubPlace>Imprinted at London :</pubPlace>
      <date>1641.</date>
     </publicationStmt>
     <notesStmt>
      <note>Explains why the Parliament felt it necessary to sit on the Lords day.</note>
      <note>With engraving of royal seal of Charles I at head of document.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>England and Wales. -- Parliament -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A83458</ep:tcp>
    <ep:estc> R205415</ep:estc>
    <ep:stc> (Thomason 669.f.3[11]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>Die Dominico 8 Aug. 1641. Resolved upon the question by both Houses of Parliament, nemine contradicente, that this ensuing declaration shall</ep:title>
    <ep:author>England and Wales. Parliament. </ep:author>
    <ep:publicationYear>1641</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>269</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-10</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2007-10</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2007-12</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2007-12</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A83458-e10">
  <body xml:id="A83458-e20">
   <pb facs="tcp:160569:1" rend="simple:additions" xml:id="A83458-001-a"/>
   <div type="Parliamentary_resolution" xml:id="A83458-e30">
    <head xml:id="A83458-e40">
     <figure xml:id="A83458-e50">
      <p xml:id="A83458-e60">
       <w lemma="dieu" pos="ffr" reg="Dieu" xml:id="A83458-001-a-0010">DIEV</w>
       <w lemma="et" pos="ffr" xml:id="A83458-001-a-0020">ET</w>
       <w lemma="mon" pos="ffr" xml:id="A83458-001-a-0030">MON</w>
       <w lemma="droit" pos="ffr" xml:id="A83458-001-a-0040">DROIT</w>
      </p>
      <p xml:id="A83458-e70">
       <w lemma="honi" pos="ffr" xml:id="A83458-001-a-0050">HONI</w>
       <w lemma="soit" pos="ffr" xml:id="A83458-001-a-0060">SOIT</w>
       <w lemma="qvi" pos="ffr" reg="x" xml:id="A83458-001-a-0070">QVI</w>
       <w lemma="mal" pos="ffr" xml:id="A83458-001-a-0080">MAL</w>
       <w lemma="y" pos="ffr" xml:id="A83458-001-a-0090">Y</w>
       <w lemma="PENSE" pos="ffr" xml:id="A83458-001-a-0100">PENSE</w>
      </p>
      <figDesc xml:id="A83458-e80">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <opener xml:id="A83458-e90">
     <dateline xml:id="A83458-e100">
      <w lemma="❧" pos="sy" xml:id="A83458-001-a-0110">❧</w>
      <date xml:id="A83458-e110">
       <w lemma="die" pos="vvb" xml:id="A83458-001-a-0120">Die</w>
       <w lemma="Dominico" pos="nn1" xml:id="A83458-001-a-0130">Dominico</w>
       <w lemma="8" pos="crd" xml:id="A83458-001-a-0140">8</w>
       <w lemma="aug." pos="ab" xml:id="A83458-001-a-0150">Aug.</w>
       <w lemma="1641." pos="crd" xml:id="A83458-001-a-0170">1641.</w>
       <pc unit="sentence" xml:id="A83458-001-a-0180"/>
      </date>
     </dateline>
     <lb xml:id="A83458-e120"/>
     <w lemma="resolve" pos="j-vn" xml:id="A83458-001-a-0190">Resolved</w>
     <w lemma="upon" pos="acp" xml:id="A83458-001-a-0200">upon</w>
     <w lemma="the" pos="d" xml:id="A83458-001-a-0210">the</w>
     <w lemma="question" pos="n1" xml:id="A83458-001-a-0220">Question</w>
     <w lemma="by" pos="acp" xml:id="A83458-001-a-0230">by</w>
     <w lemma="both" pos="d" xml:id="A83458-001-a-0240">both</w>
     <w lemma="house" pos="n2" xml:id="A83458-001-a-0250">Houses</w>
     <w lemma="of" pos="acp" xml:id="A83458-001-a-0260">of</w>
     <w lemma="parliament" pos="n1" xml:id="A83458-001-a-0270">Parliament</w>
     <pc xml:id="A83458-001-a-0280">,</pc>
     <foreign xml:id="A83458-e130" xml:lang="lat">
      <w lemma="nemine" pos="fla" xml:id="A83458-001-a-0290">Nemine</w>
      <w lemma="contradicente" pos="fla" xml:id="A83458-001-a-0300">Contradicente</w>
      <pc xml:id="A83458-001-a-0310">,</pc>
     </foreign>
     <w lemma="that" pos="cs" xml:id="A83458-001-a-0320">that</w>
     <w lemma="this" pos="d" xml:id="A83458-001-a-0330">this</w>
     <w lemma="ensue" pos="j-vg" xml:id="A83458-001-a-0340">ensuing</w>
     <w lemma="declaration" pos="n1" xml:id="A83458-001-a-0350">Declaration</w>
     <w lemma="shall" pos="vmb" xml:id="A83458-001-a-0360">shall</w>
     <w lemma="be" pos="vvi" xml:id="A83458-001-a-0370">be</w>
     <w lemma="print" pos="vvn" xml:id="A83458-001-a-0380">printed</w>
     <pc unit="sentence" xml:id="A83458-001-a-0390">.</pc>
    </opener>
    <p xml:id="A83458-e140">
     <w lemma="whereas" pos="cs" rend="decorinit" xml:id="A83458-001-a-0400">WHereas</w>
     <w lemma="both" pos="d" xml:id="A83458-001-a-0410">both</w>
     <w lemma="house" pos="n2" xml:id="A83458-001-a-0420">Houses</w>
     <w lemma="of" pos="acp" xml:id="A83458-001-a-0430">of</w>
     <w lemma="parliament" pos="n1" xml:id="A83458-001-a-0440">Parliament</w>
     <w lemma="find" pos="vvd" xml:id="A83458-001-a-0450">found</w>
     <w lemma="it" pos="pn" xml:id="A83458-001-a-0460">it</w>
     <w lemma="fit" pos="j" xml:id="A83458-001-a-0470">fit</w>
     <w lemma="to" pos="prt" xml:id="A83458-001-a-0480">to</w>
     <w lemma="sit" pos="vvi" xml:id="A83458-001-a-0490">sit</w>
     <w lemma="in" pos="acp" xml:id="A83458-001-a-0500">in</w>
     <w lemma="parliament" pos="n1" xml:id="A83458-001-a-0510">Parliament</w>
     <w lemma="upon" pos="acp" xml:id="A83458-001-a-0520">upon</w>
     <w lemma="the" pos="d" xml:id="A83458-001-a-0530">the</w>
     <w lemma="eight" pos="ord" xml:id="A83458-001-a-0540">eighth</w>
     <w lemma="day" pos="n1" xml:id="A83458-001-a-0550">day</w>
     <w lemma="of" pos="acp" xml:id="A83458-001-a-0560">of</w>
     <w lemma="August" pos="nn1" xml:id="A83458-001-a-0570">August</w>
     <pc xml:id="A83458-001-a-0580">,</pc>
     <w lemma="be" pos="vvg" xml:id="A83458-001-a-0590">being</w>
     <w lemma="the" pos="d" xml:id="A83458-001-a-0600">the</w>
     <w lemma="lord" pos="ng1" reg="Lord's" xml:id="A83458-001-a-0610">Lords</w>
     <w lemma="day" pos="n1" xml:id="A83458-001-a-0620">Day</w>
     <pc xml:id="A83458-001-a-0630">,</pc>
     <w lemma="for" pos="acp" xml:id="A83458-001-a-0640">for</w>
     <w lemma="many" pos="d" xml:id="A83458-001-a-0650">many</w>
     <w lemma="urgent" pos="j" xml:id="A83458-001-a-0660">urgent</w>
     <w lemma="and" pos="cc" xml:id="A83458-001-a-0670">and</w>
     <w lemma="unexpected" pos="j" xml:id="A83458-001-a-0680">unexpected</w>
     <w lemma="occasion" pos="n2" xml:id="A83458-001-a-0690">occasions</w>
     <w lemma="concerning" pos="acp" xml:id="A83458-001-a-0700">concerning</w>
     <w lemma="the" pos="d" xml:id="A83458-001-a-0710">the</w>
     <w lemma="safety" pos="n1" xml:id="A83458-001-a-0720">safety</w>
     <w lemma="of" pos="acp" xml:id="A83458-001-a-0730">of</w>
     <w lemma="the" pos="d" xml:id="A83458-001-a-0740">the</w>
     <w lemma="kingdom" pos="n1" xml:id="A83458-001-a-0750">Kingdom</w>
     <pc xml:id="A83458-001-a-0760">,</pc>
     <w lemma="and" pos="cc" xml:id="A83458-001-a-0770">and</w>
     <w lemma="be" pos="vvg" xml:id="A83458-001-a-0780">being</w>
     <w lemma="so" pos="av" xml:id="A83458-001-a-0790">so</w>
     <w lemma="straighten" pos="j-vn" reg="straightened" xml:id="A83458-001-a-0800">straitned</w>
     <w lemma="in" pos="acp" xml:id="A83458-001-a-0810">in</w>
     <w lemma="time" pos="n1" xml:id="A83458-001-a-0820">time</w>
     <pc xml:id="A83458-001-a-0830">,</pc>
     <w lemma="by" pos="acp" xml:id="A83458-001-a-0840">by</w>
     <w lemma="reason" pos="n1" xml:id="A83458-001-a-0850">reason</w>
     <w lemma="of" pos="acp" xml:id="A83458-001-a-0860">of</w>
     <w lemma="his" pos="po" xml:id="A83458-001-a-0870">His</w>
     <w lemma="majesty" pos="ng1" reg="majesty's" xml:id="A83458-001-a-0880">Majesties</w>
     <w lemma="resolution" pos="n1" xml:id="A83458-001-a-0890">Resolution</w>
     <w lemma="to" pos="prt" xml:id="A83458-001-a-0900">to</w>
     <w lemma="begin" pos="vvi" xml:id="A83458-001-a-0910">begin</w>
     <w lemma="his" pos="po" xml:id="A83458-001-a-0920">his</w>
     <w lemma="journey" pos="n1" xml:id="A83458-001-a-0930">Journey</w>
     <w lemma="towards" pos="acp" xml:id="A83458-001-a-0940">towards</w>
     <w lemma="Scotland" pos="nn1" xml:id="A83458-001-a-0950">Scotland</w>
     <w lemma="on" pos="acp" xml:id="A83458-001-a-0960">on</w>
     <w lemma="Monday" pos="nn1" xml:id="A83458-001-a-0970">Monday</w>
     <w lemma="follow" pos="vvg" xml:id="A83458-001-a-0980">following</w>
     <pc xml:id="A83458-001-a-0990">,</pc>
     <w lemma="early" pos="av-j" xml:id="A83458-001-a-1000">early</w>
     <w lemma="in" pos="acp" xml:id="A83458-001-a-1010">in</w>
     <w lemma="the" pos="d" xml:id="A83458-001-a-1020">the</w>
     <w lemma="morning" pos="n1" xml:id="A83458-001-a-1030">morning</w>
     <pc xml:id="A83458-001-a-1040">,</pc>
     <w lemma="it" pos="pn" xml:id="A83458-001-a-1050">it</w>
     <w lemma="be" pos="vvd" xml:id="A83458-001-a-1060">was</w>
     <w lemma="not" pos="xx" xml:id="A83458-001-a-1070">not</w>
     <w lemma="possible" pos="j" xml:id="A83458-001-a-1080">possible</w>
     <w lemma="for" pos="acp" xml:id="A83458-001-a-1090">for</w>
     <w lemma="to" pos="prt" xml:id="A83458-001-a-1100">to</w>
     <w lemma="settle" pos="vvi" xml:id="A83458-001-a-1110">settle</w>
     <w lemma="and" pos="cc" xml:id="A83458-001-a-1120">and</w>
     <w lemma="order" pos="vvi" xml:id="A83458-001-a-1130">order</w>
     <w lemma="the" pos="d" xml:id="A83458-001-a-1140">the</w>
     <w lemma="affair" pos="n2" xml:id="A83458-001-a-1150">Affairs</w>
     <w lemma="of" pos="acp" xml:id="A83458-001-a-1160">of</w>
     <w lemma="the" pos="d" xml:id="A83458-001-a-1170">the</w>
     <w lemma="kingdom" pos="n1" xml:id="A83458-001-a-1180">Kingdom</w>
     <pc xml:id="A83458-001-a-1190">,</pc>
     <w lemma="either" pos="av-d" xml:id="A83458-001-a-1200">either</w>
     <w lemma="for" pos="acp" xml:id="A83458-001-a-1210">for</w>
     <w lemma="the" pos="d" xml:id="A83458-001-a-1220">the</w>
     <w lemma="government" pos="n1" xml:id="A83458-001-a-1230">Government</w>
     <w lemma="thereof" pos="av" xml:id="A83458-001-a-1240">thereof</w>
     <w lemma="in" pos="acp" xml:id="A83458-001-a-1250">in</w>
     <w lemma="the" pos="d" xml:id="A83458-001-a-1260">the</w>
     <w lemma="king" pos="ng1" reg="King's" xml:id="A83458-001-a-1270">Kings</w>
     <w lemma="absence" pos="n1" xml:id="A83458-001-a-1280">absence</w>
     <pc xml:id="A83458-001-a-1290">,</pc>
     <w lemma="or" pos="cc" xml:id="A83458-001-a-1300">or</w>
     <w lemma="for" pos="acp" xml:id="A83458-001-a-1310">for</w>
     <w lemma="the" pos="d" xml:id="A83458-001-a-1320">the</w>
     <w lemma="present" pos="j" xml:id="A83458-001-a-1330">present</w>
     <w lemma="safety" pos="n1" xml:id="A83458-001-a-1340">safety</w>
     <pc xml:id="A83458-001-a-1350">,</pc>
     <w lemma="as" pos="acp" xml:id="A83458-001-a-1360">as</w>
     <w lemma="be" pos="vvd" xml:id="A83458-001-a-1370">was</w>
     <w lemma="requisite" pos="j" xml:id="A83458-001-a-1380">requisite</w>
     <pc xml:id="A83458-001-a-1390">:</pc>
     <w lemma="upon" pos="acp" xml:id="A83458-001-a-1400">Upon</w>
     <w lemma="these" pos="d" xml:id="A83458-001-a-1410">these</w>
     <w lemma="present" pos="j" xml:id="A83458-001-a-1420">present</w>
     <w lemma="necessity" pos="n2" xml:id="A83458-001-a-1430">necessities</w>
     <pc xml:id="A83458-001-a-1440">,</pc>
     <w lemma="though" pos="cs" xml:id="A83458-001-a-1450">though</w>
     <w lemma="the" pos="d" xml:id="A83458-001-a-1460">the</w>
     <w lemma="house" pos="n2" xml:id="A83458-001-a-1470">Houses</w>
     <w lemma="think" pos="vvd" xml:id="A83458-001-a-1480">thought</w>
     <w lemma="it" pos="pn" xml:id="A83458-001-a-1490">it</w>
     <w lemma="necessary" pos="j" xml:id="A83458-001-a-1500">necessary</w>
     <w lemma="to" pos="prt" xml:id="A83458-001-a-1510">to</w>
     <w lemma="sit" pos="vvi" xml:id="A83458-001-a-1520">sit</w>
     <pc xml:id="A83458-001-a-1530">,</pc>
     <w lemma="yet" pos="av" xml:id="A83458-001-a-1540">yet</w>
     <w lemma="the" pos="d" xml:id="A83458-001-a-1550">the</w>
     <w lemma="lord" pos="n2" xml:id="A83458-001-a-1560">Lords</w>
     <w lemma="and" pos="cc" xml:id="A83458-001-a-1570">and</w>
     <w lemma="commons" pos="n2" xml:id="A83458-001-a-1580">Commons</w>
     <w lemma="now" pos="av" xml:id="A83458-001-a-1590">now</w>
     <w lemma="assemble" pos="vvn" xml:id="A83458-001-a-1600">assembled</w>
     <w lemma="in" pos="acp" xml:id="A83458-001-a-1610">in</w>
     <w lemma="parliament" pos="n1" xml:id="A83458-001-a-1620">Parliament</w>
     <w lemma="think" pos="vvb" xml:id="A83458-001-a-1630">think</w>
     <w lemma="it" pos="pn" xml:id="A83458-001-a-1640">it</w>
     <w lemma="meet" pos="vvi" xml:id="A83458-001-a-1650">meet</w>
     <w lemma="to" pos="prt" xml:id="A83458-001-a-1660">to</w>
     <w lemma="declare" pos="vvi" xml:id="A83458-001-a-1670">declare</w>
     <pc xml:id="A83458-001-a-1680">,</pc>
     <w lemma="that" pos="cs" xml:id="A83458-001-a-1690">that</w>
     <w lemma="they" pos="pns" xml:id="A83458-001-a-1700">they</w>
     <w lemma="will" pos="vmd" xml:id="A83458-001-a-1710">would</w>
     <w lemma="not" pos="xx" xml:id="A83458-001-a-1720">not</w>
     <w lemma="have" pos="vvi" xml:id="A83458-001-a-1730">have</w>
     <w lemma="do" pos="vvn" xml:id="A83458-001-a-1740">done</w>
     <w lemma="this" pos="d" xml:id="A83458-001-a-1750">this</w>
     <pc xml:id="A83458-001-a-1760">,</pc>
     <w lemma="but" pos="acp" xml:id="A83458-001-a-1770">but</w>
     <w lemma="upon" pos="acp" xml:id="A83458-001-a-1780">upon</w>
     <w lemma="inevitable" pos="j" xml:id="A83458-001-a-1790">inevitable</w>
     <w lemma="necessity" pos="n1" xml:id="A83458-001-a-1800">necessity</w>
     <pc xml:id="A83458-001-a-1810">,</pc>
     <w lemma="the" pos="d" xml:id="A83458-001-a-1820">the</w>
     <w lemma="peace" pos="n1" xml:id="A83458-001-a-1830">Peace</w>
     <w lemma="and" pos="cc" xml:id="A83458-001-a-1840">and</w>
     <w lemma="safety" pos="n1" xml:id="A83458-001-a-1850">Safety</w>
     <w lemma="both" pos="av-d" xml:id="A83458-001-a-1860">both</w>
     <w lemma="of" pos="acp" xml:id="A83458-001-a-1870">of</w>
     <w lemma="church" pos="n1" xml:id="A83458-001-a-1880">Church</w>
     <w lemma="and" pos="cc" xml:id="A83458-001-a-1890">and</w>
     <w lemma="state" pos="n1" xml:id="A83458-001-a-1900">State</w>
     <w lemma="be" pos="vvg" xml:id="A83458-001-a-1910">being</w>
     <w lemma="so" pos="av" xml:id="A83458-001-a-1920">so</w>
     <w lemma="deep" pos="av-j" xml:id="A83458-001-a-1930">deeply</w>
     <w lemma="concern" pos="vvn" xml:id="A83458-001-a-1940">concerned</w>
     <pc xml:id="A83458-001-a-1950">,</pc>
     <w lemma="which" pos="crq" xml:id="A83458-001-a-1960">which</w>
     <w lemma="they" pos="pns" xml:id="A83458-001-a-1970">they</w>
     <w lemma="do" pos="vvb" xml:id="A83458-001-a-1980">do</w>
     <w lemma="hereby" pos="av" xml:id="A83458-001-a-1990">hereby</w>
     <w lemma="declare" pos="vvi" xml:id="A83458-001-a-2000">declare</w>
     <pc xml:id="A83458-001-a-2010">,</pc>
     <w lemma="to" pos="acp" xml:id="A83458-001-a-2020">to</w>
     <w lemma="the" pos="d" xml:id="A83458-001-a-2030">the</w>
     <w lemma="end" pos="n1" xml:id="A83458-001-a-2040">end</w>
     <w lemma="that" pos="cs" xml:id="A83458-001-a-2050">that</w>
     <w lemma="neither" pos="dx" xml:id="A83458-001-a-2060">neither</w>
     <w lemma="any" pos="d" xml:id="A83458-001-a-2070">any</w>
     <w lemma="other" pos="d" xml:id="A83458-001-a-2080">other</w>
     <w lemma="inferior" pos="j" reg="inferior" xml:id="A83458-001-a-2090">inferiour</w>
     <w lemma="court" pos="n1" xml:id="A83458-001-a-2100">Court</w>
     <pc xml:id="A83458-001-a-2110">,</pc>
     <w lemma="or" pos="cc" xml:id="A83458-001-a-2120">or</w>
     <w lemma="council" pos="n1" reg="council" xml:id="A83458-001-a-2130">Councell</w>
     <pc xml:id="A83458-001-a-2140">,</pc>
     <w lemma="or" pos="cc" xml:id="A83458-001-a-2150">or</w>
     <w lemma="any" pos="d" xml:id="A83458-001-a-2160">any</w>
     <w lemma="other" pos="d" xml:id="A83458-001-a-2170">other</w>
     <w lemma="person" pos="n1" xml:id="A83458-001-a-2180">person</w>
     <pc xml:id="A83458-001-a-2190">,</pc>
     <w lemma="may" pos="vmb" xml:id="A83458-001-a-2200">may</w>
     <w lemma="draw" pos="vvi" xml:id="A83458-001-a-2210">draw</w>
     <w lemma="this" pos="d" xml:id="A83458-001-a-2220">this</w>
     <w lemma="into" pos="acp" xml:id="A83458-001-a-2230">into</w>
     <w lemma="example" pos="n1" xml:id="A83458-001-a-2240">Example</w>
     <pc xml:id="A83458-001-a-2250">,</pc>
     <w lemma="or" pos="cc" xml:id="A83458-001-a-2260">or</w>
     <w lemma="make" pos="vvi" xml:id="A83458-001-a-2270">make</w>
     <w lemma="use" pos="n1" xml:id="A83458-001-a-2280">use</w>
     <w lemma="of" pos="acp" xml:id="A83458-001-a-2290">of</w>
     <w lemma="it" pos="pn" xml:id="A83458-001-a-2300">it</w>
     <pc xml:id="A83458-001-a-2310">,</pc>
     <w lemma="for" pos="acp" xml:id="A83458-001-a-2320">for</w>
     <w lemma="their" pos="po" xml:id="A83458-001-a-2330">their</w>
     <w lemma="encouragement" pos="n1" xml:id="A83458-001-a-2340">encouragement</w>
     <w lemma="in" pos="acp" xml:id="A83458-001-a-2350">in</w>
     <w lemma="neglect" pos="vvg" xml:id="A83458-001-a-2360">neglecting</w>
     <w lemma="the" pos="d" xml:id="A83458-001-a-2370">the</w>
     <w lemma="due" pos="j" xml:id="A83458-001-a-2380">due</w>
     <w lemma="observation" pos="n1" xml:id="A83458-001-a-2390">observation</w>
     <w lemma="of" pos="acp" xml:id="A83458-001-a-2400">of</w>
     <w lemma="the" pos="d" xml:id="A83458-001-a-2410">the</w>
     <w lemma="lord" pos="ng1" reg="Lord's" xml:id="A83458-001-a-2420">Lords</w>
     <w lemma="day" pos="n1" xml:id="A83458-001-a-2430">Day</w>
     <pc unit="sentence" xml:id="A83458-001-a-2440">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="A83458-e150">
   <div type="colophon" xml:id="A83458-e160">
    <p xml:id="A83458-e170">
     <pc xml:id="A83458-001-a-2450">¶</pc>
     <w lemma="imprint" pos="vvn" xml:id="A83458-001-a-2460">Imprinted</w>
     <w lemma="at" pos="acp" xml:id="A83458-001-a-2470">at</w>
     <w lemma="London" pos="nn1" xml:id="A83458-001-a-2480">London</w>
     <w lemma="by" pos="acp" xml:id="A83458-001-a-2490">by</w>
     <w lemma="Robert" pos="nn1" xml:id="A83458-001-a-2500">Robert</w>
     <w lemma="Barker" pos="nn1" xml:id="A83458-001-a-2510">Barker</w>
     <pc xml:id="A83458-001-a-2520">,</pc>
     <w lemma="printer" pos="n1" xml:id="A83458-001-a-2530">Printer</w>
     <w lemma="to" pos="acp" xml:id="A83458-001-a-2540">to</w>
     <w lemma="the" pos="d" xml:id="A83458-001-a-2550">the</w>
     <w lemma="king" pos="n2" xml:id="A83458-001-a-2560">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A83458-001-a-2570">most</w>
     <w lemma="excellent" pos="j" xml:id="A83458-001-a-2580">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A83458-001-a-2590">Majesty</w>
     <pc xml:id="A83458-001-a-2600">:</pc>
     <w lemma="and" pos="cc" xml:id="A83458-001-a-2610">And</w>
     <w lemma="by" pos="acp" xml:id="A83458-001-a-2620">by</w>
     <w lemma="the" pos="d" xml:id="A83458-001-a-2630">the</w>
     <w lemma="assign" pos="vvz" reg="assigns" xml:id="A83458-001-a-2640">Assignes</w>
     <w lemma="of" pos="acp" xml:id="A83458-001-a-2650">of</w>
     <w lemma="JOHN" pos="nn1" xml:id="A83458-001-a-2660">JOHN</w>
     <w lemma="bill" pos="nn1" xml:id="A83458-001-a-2670">BILL</w>
     <pc unit="sentence" xml:id="A83458-001-a-2680">.</pc>
     <w lemma="1641." pos="crd" xml:id="A83458-001-a-2690">1641.</w>
     <pc unit="sentence" xml:id="A83458-001-a-2700"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
