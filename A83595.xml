<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A83595">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Votes of Parliament for setting apart a day of publique fasting and humiliation. Wednesday the ninth of February, 1652.</title>
    <author>England and Wales. Parliament.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A83595 of text R211649 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.16[89]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A83595</idno>
    <idno type="STC">Wing E2451</idno>
    <idno type="STC">Thomason 669.f.16[89]</idno>
    <idno type="STC">ESTC R211649</idno>
    <idno type="EEBO-CITATION">99870359</idno>
    <idno type="PROQUEST">99870359</idno>
    <idno type="VID">163242</idno>
    <idno type="PROQUESTGOID">2248506651</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A83595)</note>
    <note>Transcribed from: (Early English Books Online ; image set 163242)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f16[89])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Votes of Parliament for setting apart a day of publique fasting and humiliation. Wednesday the ninth of February, 1652.</title>
      <author>England and Wales. Parliament.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by John Field, Printer to the Parliament of England,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1652 [i.e. 1653]</date>
     </publicationStmt>
     <notesStmt>
      <note>Signed: Hen: Scobell, Cleric. Parliamenti.</note>
      <note>With Parliamentary seal at head of text.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Fasts and feasts -- England -- Early works to 1800.</term>
     <term>Great Britain -- Politics and government -- 1649-1660 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A83595</ep:tcp>
    <ep:estc> R211649</ep:estc>
    <ep:stc> (Thomason 669.f.16[89]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>Votes of Parliament for setting apart a day of publique fasting and humiliation. Wednesday the ninth of February, 1652.</ep:title>
    <ep:author>England and Wales. Parliament. </ep:author>
    <ep:publicationYear>1653</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>161</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-11</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2007-11</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2007-12</date><label>Elspeth Healey</label>
        Sampled and proofread
      </change>
   <change><date>2007-12</date><label>Elspeth Healey</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A83595-e10">
  <body xml:id="A83595-e20">
   <pb facs="tcp:163242:1" rend="simple:additions" xml:id="A83595-001-a"/>
   <div type="Parliamentary_resolution" xml:id="A83595-e30">
    <head xml:id="A83595-e40">
     <figure xml:id="A83595-e50"/>
     <lb xml:id="A83595-e60"/>
     <w lemma="vote" pos="n2" xml:id="A83595-001-a-0010">Votes</w>
     <w lemma="of" pos="acp" xml:id="A83595-001-a-0020">of</w>
     <w lemma="parliament" pos="n1" xml:id="A83595-001-a-0030">Parliament</w>
     <w lemma="for" pos="acp" xml:id="A83595-001-a-0040">For</w>
     <w lemma="set" pos="vvg" xml:id="A83595-001-a-0050">setting</w>
     <w lemma="apart" pos="av" xml:id="A83595-001-a-0060">apart</w>
     <w lemma="a" pos="d" xml:id="A83595-001-a-0070">a</w>
     <w lemma="day" pos="n1" xml:id="A83595-001-a-0080">Day</w>
     <w lemma="of" pos="acp" xml:id="A83595-001-a-0090">of</w>
     <w lemma="public" pos="j" reg="public" xml:id="A83595-001-a-0100">Publique</w>
     <w lemma="fast" pos="n1-vg" xml:id="A83595-001-a-0110">FASTING</w>
     <w lemma="and" pos="cc" xml:id="A83595-001-a-0120">and</w>
     <w lemma="humiliation" pos="n1" xml:id="A83595-001-a-0130">HUMILIATION</w>
     <pc unit="sentence" xml:id="A83595-001-a-0140">.</pc>
    </head>
    <opener xml:id="A83595-e70">
     <dateline xml:id="A83595-e80">
      <date xml:id="A83595-e90">
       <w lemma="wednesday" pos="nn1" xml:id="A83595-001-a-0150">Wednesday</w>
       <w lemma="the" pos="d" xml:id="A83595-001-a-0160">the</w>
       <w lemma="nine" pos="ord" xml:id="A83595-001-a-0170">Ninth</w>
       <w lemma="of" pos="acp" xml:id="A83595-001-a-0180">of</w>
       <hi xml:id="A83595-e100">
        <w lemma="February" pos="nn1" xml:id="A83595-001-a-0190">February</w>
        <pc xml:id="A83595-001-a-0200">,</pc>
        <w lemma="1652." pos="crd" xml:id="A83595-001-a-0210">1652.</w>
        <pc unit="sentence" xml:id="A83595-001-a-0220"/>
       </hi>
      </date>
     </dateline>
     <w lemma="resolve" pos="j-vn" xml:id="A83595-001-a-0230">Resolved</w>
     <w lemma="upon" pos="acp" xml:id="A83595-001-a-0240">upon</w>
     <w lemma="the" pos="d" xml:id="A83595-001-a-0250">the</w>
     <w lemma="question" pos="n1" xml:id="A83595-001-a-0260">Question</w>
     <w lemma="by" pos="acp" xml:id="A83595-001-a-0270">by</w>
     <w lemma="the" pos="d" xml:id="A83595-001-a-0280">the</w>
     <w lemma="parliament" pos="n1" xml:id="A83595-001-a-0290">Parliament</w>
     <pc xml:id="A83595-001-a-0300">,</pc>
    </opener>
    <p xml:id="A83595-e110">
     <w lemma="that" pos="cs" rend="decorinit" xml:id="A83595-001-a-0310">THat</w>
     <w lemma="Thursday" pos="nn1" xml:id="A83595-001-a-0320">Thursday</w>
     <w lemma="the" pos="d" xml:id="A83595-001-a-0330">the</w>
     <w lemma="three" pos="ord" xml:id="A83595-001-a-0340">third</w>
     <w lemma="day" pos="n1" xml:id="A83595-001-a-0350">day</w>
     <w lemma="of" pos="acp" xml:id="A83595-001-a-0360">of</w>
     <hi xml:id="A83595-e120">
      <w lemma="march" pos="nn1" xml:id="A83595-001-a-0370">March</w>
     </hi>
     <w lemma="next" pos="ord" xml:id="A83595-001-a-0380">next</w>
     <pc xml:id="A83595-001-a-0390">,</pc>
     <w lemma="be" pos="vvb" xml:id="A83595-001-a-0400">be</w>
     <w lemma="set" pos="vvn" xml:id="A83595-001-a-0410">set</w>
     <w lemma="apart" pos="av" xml:id="A83595-001-a-0420">apart</w>
     <w lemma="for" pos="acp" xml:id="A83595-001-a-0430">for</w>
     <w lemma="a" pos="d" xml:id="A83595-001-a-0440">a</w>
     <w lemma="day" pos="n1" xml:id="A83595-001-a-0450">Day</w>
     <w lemma="of" pos="acp" xml:id="A83595-001-a-0460">of</w>
     <w lemma="public" pos="j" reg="public" xml:id="A83595-001-a-0470">Publique</w>
     <w lemma="fast" pos="n1-vg" xml:id="A83595-001-a-0480">Fasting</w>
     <w lemma="and" pos="cc" xml:id="A83595-001-a-0490">and</w>
     <w lemma="humiliation" pos="n1" xml:id="A83595-001-a-0500">Humiliation</w>
     <pc xml:id="A83595-001-a-0510">,</pc>
     <w lemma="to" pos="prt" xml:id="A83595-001-a-0520">to</w>
     <w lemma="be" pos="vvi" xml:id="A83595-001-a-0530">be</w>
     <w lemma="observe" pos="vvn" xml:id="A83595-001-a-0540">observed</w>
     <w lemma="throughout" pos="acp" xml:id="A83595-001-a-0550">throughout</w>
     <w lemma="this" pos="d" xml:id="A83595-001-a-0560">this</w>
     <w lemma="whole" pos="j" xml:id="A83595-001-a-0570">whole</w>
     <w lemma="nation" pos="n1" xml:id="A83595-001-a-0580">Nation</w>
     <pc xml:id="A83595-001-a-0590">,</pc>
     <w lemma="to" pos="prt" xml:id="A83595-001-a-0600">to</w>
     <w lemma="seek" pos="vvi" xml:id="A83595-001-a-0610">seek</w>
     <w lemma="unto" pos="acp" xml:id="A83595-001-a-0620">unto</w>
     <w lemma="almighty" pos="j" xml:id="A83595-001-a-0630">Almighty</w>
     <w lemma="God" pos="nn1" xml:id="A83595-001-a-0640">God</w>
     <w lemma="for" pos="acp" xml:id="A83595-001-a-0650">for</w>
     <w lemma="a" pos="d" xml:id="A83595-001-a-0660">a</w>
     <w lemma="blessing" pos="n1" xml:id="A83595-001-a-0670">Blessing</w>
     <w lemma="upon" pos="acp" xml:id="A83595-001-a-0680">upon</w>
     <w lemma="the" pos="d" xml:id="A83595-001-a-0690">the</w>
     <w lemma="counsel" pos="n2" reg="counsels" xml:id="A83595-001-a-0700">Councels</w>
     <w lemma="of" pos="acp" xml:id="A83595-001-a-0710">of</w>
     <w lemma="this" pos="d" xml:id="A83595-001-a-0720">this</w>
     <w lemma="commonwealth" pos="n1" xml:id="A83595-001-a-0730">Commonwealth</w>
     <pc xml:id="A83595-001-a-0740">,</pc>
     <w lemma="and" pos="cc" xml:id="A83595-001-a-0750">and</w>
     <w lemma="upon" pos="acp" xml:id="A83595-001-a-0760">upon</w>
     <w lemma="their" pos="po" xml:id="A83595-001-a-0770">their</w>
     <w lemma="force" pos="n2" xml:id="A83595-001-a-0780">Forces</w>
     <w lemma="by" pos="acp" xml:id="A83595-001-a-0790">by</w>
     <w lemma="sea" pos="n1" xml:id="A83595-001-a-0800">Sea</w>
     <w lemma="and" pos="cc" xml:id="A83595-001-a-0810">and</w>
     <w lemma="land" pos="n1" xml:id="A83595-001-a-0820">Land</w>
     <pc unit="sentence" xml:id="A83595-001-a-0830">.</pc>
    </p>
    <p xml:id="A83595-e130">
     <w lemma="resolve" pos="j-vn" xml:id="A83595-001-a-0840">Resolved</w>
     <w lemma="upon" pos="acp" xml:id="A83595-001-a-0850">upon</w>
     <w lemma="the" pos="d" xml:id="A83595-001-a-0860">the</w>
     <w lemma="question" pos="n1" xml:id="A83595-001-a-0870">Question</w>
     <w lemma="by" pos="acp" xml:id="A83595-001-a-0880">by</w>
     <w lemma="the" pos="d" xml:id="A83595-001-a-0890">the</w>
     <w lemma="parliament" pos="n1" xml:id="A83595-001-a-0900">Parliament</w>
     <pc xml:id="A83595-001-a-0910">,</pc>
    </p>
    <p xml:id="A83595-e140">
     <hi xml:id="A83595-e150">
      <w lemma="that" pos="cs" xml:id="A83595-001-a-0920">That</w>
      <w lemma="this" pos="d" xml:id="A83595-001-a-0930">this</w>
      <w lemma="vote" pos="n1" xml:id="A83595-001-a-0940">Vote</w>
      <w lemma="be" pos="vvi" xml:id="A83595-001-a-0950">be</w>
      <w lemma="print" pos="vvn" xml:id="A83595-001-a-0960">Printed</w>
      <w lemma="and" pos="cc" xml:id="A83595-001-a-0970">and</w>
      <w lemma="publish" pos="vvn" xml:id="A83595-001-a-0980">Published</w>
      <pc xml:id="A83595-001-a-0990">,</pc>
      <w lemma="and" pos="cc" xml:id="A83595-001-a-1000">and</w>
      <w lemma="copy" pos="n2" xml:id="A83595-001-a-1010">Copies</w>
      <w lemma="thereof" pos="av" xml:id="A83595-001-a-1020">thereof</w>
      <w lemma="send" pos="vvn" xml:id="A83595-001-a-1030">sent</w>
      <w lemma="to" pos="acp" xml:id="A83595-001-a-1040">to</w>
      <w lemma="the" pos="d" xml:id="A83595-001-a-1050">the</w>
      <w lemma="sheriff" pos="n2" xml:id="A83595-001-a-1060">Sheriffs</w>
      <w lemma="of" pos="acp" xml:id="A83595-001-a-1070">of</w>
      <w lemma="every" pos="d" xml:id="A83595-001-a-1080">every</w>
      <w lemma="county" pos="n1" xml:id="A83595-001-a-1090">County</w>
      <pc xml:id="A83595-001-a-1100">;</pc>
      <w lemma="and" pos="cc" xml:id="A83595-001-a-1110">and</w>
      <w lemma="that" pos="cs" xml:id="A83595-001-a-1120">that</w>
      <w lemma="they" pos="pns" xml:id="A83595-001-a-1130">they</w>
      <w lemma="be" pos="vvb" xml:id="A83595-001-a-1140">be</w>
      <w lemma="require" pos="vvn" xml:id="A83595-001-a-1150">required</w>
      <w lemma="to" pos="prt" xml:id="A83595-001-a-1160">to</w>
      <w lemma="send" pos="vvi" xml:id="A83595-001-a-1170">send</w>
      <w lemma="the" pos="d" xml:id="A83595-001-a-1180">the</w>
      <w lemma="same" pos="d" xml:id="A83595-001-a-1190">same</w>
      <w lemma="unto" pos="acp" xml:id="A83595-001-a-1200">unto</w>
      <w lemma="the" pos="d" xml:id="A83595-001-a-1210">the</w>
      <w lemma="minister" pos="n2" xml:id="A83595-001-a-1220">Ministers</w>
      <w lemma="of" pos="acp" xml:id="A83595-001-a-1230">of</w>
      <w lemma="the" pos="d" xml:id="A83595-001-a-1240">the</w>
      <w lemma="several" pos="j" xml:id="A83595-001-a-1250">several</w>
      <w lemma="parish" pos="n2" xml:id="A83595-001-a-1260">Parishes</w>
      <pc xml:id="A83595-001-a-1270">,</pc>
      <w lemma="that" pos="cs" xml:id="A83595-001-a-1280">that</w>
      <w lemma="notice" pos="n1" xml:id="A83595-001-a-1290">notice</w>
      <w lemma="thereof" pos="av" xml:id="A83595-001-a-1300">thereof</w>
      <w lemma="may" pos="vmb" xml:id="A83595-001-a-1310">may</w>
      <w lemma="be" pos="vvi" xml:id="A83595-001-a-1320">be</w>
      <w lemma="give" pos="vvn" xml:id="A83595-001-a-1330">given</w>
      <w lemma="in" pos="acp" xml:id="A83595-001-a-1340">in</w>
      <w lemma="convenient" pos="j" xml:id="A83595-001-a-1350">convenient</w>
      <w lemma="time" pos="n1" xml:id="A83595-001-a-1360">time</w>
      <pc unit="sentence" xml:id="A83595-001-a-1370">.</pc>
     </hi>
    </p>
    <closer xml:id="A83595-e160">
     <signed xml:id="A83595-e170">
      <w lemma="hen" pos="ab" xml:id="A83595-001-a-1380">Hen:</w>
      <w lemma="Scobell" pos="nn1" xml:id="A83595-001-a-1400">Scobell</w>
      <pc xml:id="A83595-001-a-1410">,</pc>
      <w lemma="cleric" pos="n1" xml:id="A83595-001-a-1420">Cleric</w>
      <pc unit="sentence" xml:id="A83595-001-a-1430">.</pc>
      <w lemma="parliamenti" pos="fla" xml:id="A83595-001-a-1440">Parliamenti</w>
      <pc unit="sentence" xml:id="A83595-001-a-1450">.</pc>
     </signed>
    </closer>
   </div>
  </body>
  <back xml:id="A83595-e180">
   <div type="colophon" xml:id="A83595-e190">
    <p xml:id="A83595-e200">
     <hi xml:id="A83595-e210">
      <w lemma="london" pos="nn1" xml:id="A83595-001-a-1460">London</w>
      <pc xml:id="A83595-001-a-1470">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A83595-001-a-1480">Printed</w>
     <w lemma="by" pos="acp" xml:id="A83595-001-a-1490">by</w>
     <hi xml:id="A83595-e220">
      <w lemma="John" pos="nn1" xml:id="A83595-001-a-1500">John</w>
      <w lemma="field" pos="nn1" xml:id="A83595-001-a-1510">Field</w>
      <pc xml:id="A83595-001-a-1520">,</pc>
     </hi>
     <w lemma="printer" pos="n1" xml:id="A83595-001-a-1530">Printer</w>
     <w lemma="to" pos="acp" xml:id="A83595-001-a-1540">to</w>
     <w lemma="the" pos="d" xml:id="A83595-001-a-1550">the</w>
     <w lemma="parliament" pos="n1" xml:id="A83595-001-a-1560">Parliament</w>
     <w lemma="of" pos="acp" xml:id="A83595-001-a-1570">of</w>
     <hi xml:id="A83595-e230">
      <w lemma="England" pos="nn1" xml:id="A83595-001-a-1580">England</w>
      <pc unit="sentence" xml:id="A83595-001-a-1590">.</pc>
     </hi>
     <w lemma="1652." pos="crd" xml:id="A83595-001-a-1600">1652.</w>
     <pc unit="sentence" xml:id="A83595-001-a-1610"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
